{
  imports = [
    ./initramfs.nix
    ./env.nix
    ./net.nix
  ];
}
